<?php

/**
 * @file
 * Defines a field type for referencing Drupal paths.
 */

/**
 * Implementation of hook_menu().
 */
function pathreference_menu() {
  $items['pathreference/autocomplete'] = array(
    'title' => 'Autocomplete pathreference',
    'page callback' => 'pathreference_autocomplete',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Checks whether $path matches against $allowed_paths.
 *
 * Examples:
 *   pathreference_match('some/path', 'some/%') == TRUE
 *   pathreference_match('some/path/%/containing/wildcard', 'some/%') == TRUE
 *
 * @param $path
 * @param $allowed_paths
 *
 * @return
 *   Boolean whether $path matches against $allowed_paths.
 */
function pathreference_match($path, $allowed_paths) {
  if (!is_array($allowed_paths)) {
    $allowed_paths = array($allowed_paths);
  }

  $parts = explode('/', $path);
  $n_parts = count($parts);

  foreach ($allowed_paths as $allowed_path) {
    $a_parts = explode('/', $allowed_path);
    $a_n_parts = count($a_parts);
    $a_last = $a_parts[$a_n_parts-1];

    $max = max($n_parts, $a_n_parts);
    for ($i=0; $i<$max; $i++) {
      if (isset($parts[$i])) {
        if (isset($a_parts[$i])) {
          if ($parts[$i] != $a_parts[$i] && $a_parts[$i] != '%') {
            continue;
          }
        }elseif ($a_last != '%') {
          continue;
        }
      }
      elseif ($a_last != '%') {
        continue;
      }
    }
    return TRUE;
  }

  return empty($allowed_paths);
}

/**********************************************************************
 * Field Type API
 **********************************************************************/

/**
 * Implementation of hook_field_info().
 */
function pathreference_field_info() {
  return array(
    'pathreference' => array(
      'label' => t('Path reference'),
      'description' => t('This field stores references to paths.'),
      'settings' => array('allowed_paths'),
      'default_widget' => 'pathreference_autocomplete',
      'default_formatter' => 'pathreference_default',
    ),
    'pathreference_wildcards' => array(
      'label' => t('Path reference with wildcards'),
      'description' => t('This field stores references to paths.'),
      'settings' => array('allowed_paths'),
      'default_widget' => 'pathreference_autocomplete',
      'default_formatter' => 'pathreference_default',
    ),
  );
}

/**
 * Implements hook_field_settings_form().
 */
function pathreference_field_settings_form($field, $instance, $has_data) {
  $defaults = field_info_field_settings($field['type']);
  $settings = array_merge($defaults, $field['settings']);

  $form['allowed_paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Allowed paths list'),
    '#default_value' => $settings['allowed_paths'],
    '#required' => FALSE,
    '#rows' => 10,
    '#description' => t('The possible paths this field can contain. Enter one path per line. Wildcards can be used for more flexibility.'),
  );

  return $form;
}

/**
 * Implements hook_field_validate().
 *
 * Possible error codes:
 * - 'pathreference_max_parts': The path contains to many parts.
 * - 'pathreference_path_match': The path does not match any allowed path.
 */
function pathreference_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  $allowed_paths = pathreference_allowed_paths($field);

  foreach ($items as $delta => $item) {
    if (!empty($item['path'])) {
      $parts = explode('/', $item['path']);

      if (count($parts) > MENU_MAX_PARTS) {
        $errors[$field['field_name']][$langcode][$delta][] = array(
          'error' => "pathreference_max_parts",
          'message' => t('%name : Path contains to many parts. Maximum number of parts is %max_parts.', array('%name' => $instance['label'], '%max_parts' => MENU_MAX_PARTS)),
        );
      }

      if ($field['type'] == 'pathreference' && array_search('%', $parts)) {
        $errors[$field['field_name']][$langcode][$delta][] = array(
          'error' => "pathreference_wildcards",
          'message' => t('%name : Usage of wildcards is not allowed', array('%name' => $instance['label'])),
        );
      }

      if (!empty($allowed_paths) && !pathreference_match($item['path'], $allowed_paths)) {
        $errors[$field['field_name']][$langcode][$delta][] = array(
          'error' => "pathreference_path_match",
          'message' => t('%name : Path is not allowed.', array('%name' => $instance['label'])),
        );
      }
    }
  }
}

/**
 * Implements hook_field_is_empty().
 */
function pathreference_field_is_empty($item, $field) {
  return !is_array($item) || empty($item['path']);
}

/**
 * Implements hook_field_presave().
 */
function pathreference_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  foreach ($items as $delta => $item) {
    $path = $item['path'];
    $parts = explode('/', $path);
    $number_parts = count($parts);

    $items[$delta]['fit'] = 0;
    $items[$delta]['number_parts'] = min($number_parts, MENU_MAX_PARTS);

    $last = $parts[min($number_parts, MENU_MAX_PARTS)-1];
    for ($i=0; $i<MENU_MAX_PARTS; $i++) {
      if (isset($parts[$i])) {
        $value = $parts[$i];

        $items[$delta]['fit'] <<= 1;
        if ($value != '%') {
          $items[$delta]['fit'] |= 1;
        }
      }
      elseif ($last != '%') {
        $value = '';
      }
      else {
        $value = '%';
      }

      $items[$delta]['p'.$i] = $value;
    }
  }
}

/**
 * Implementation of hook_field_formatter_info().
 */
function pathreference_field_formatter_info() {
  return array(
    'pathreference_plain' => array(
      'label' => t('Path'),
      'field types' => array('pathreference', 'pathreference_wildcards'),
    ),
    'pathreference_link' => array(
      'label' => t('Path (as link)'),
      'field types' => array('pathreference'),
    ),
    'pathreference_fit' => array(
      'label' => t('Fitness'),
      'field types' => array('pathreference_wildcards'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function pathreference_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'pathreference_plain':
      foreach ($items as $delta => $item) {
        $element[$delta] = array('#markup' => check_plain($item['path']));
      }
      break;

    case 'pathreference_link':
      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          '#type' => 'link',
          '#title' => $item['path'],
          '#href' => $item['path'],
          '#options' => array('attributes' => array()),
        );
      }
      break;

    case 'pathreference_fit':
      foreach ($items as $delta => $item) {
        $element[$delta] = array('#markup' => $item['fit']);
      }
      break;
  }

  return $element;
}

/**
 * Implementation of hook_field_widget_info().
 */
function pathreference_field_widget_info() {
  return array(
    'pathreference_autocomplete' => array(
      'label' => 'Autocomplete',
      'field types' => array('pathreference'),
      'settings' => array(
        'size' => 60,
        'autocomplete_path' => 'pathreference/autocomplete',
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_error().
 */
function pathreference_field_widget_error($element, $error, $form, &$form_state) {
  form_error($element, $error['message']);
}

/**
 * Implements hook_field_widget_form().
 */
function pathreference_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

  $element += array(
    '#type' => 'textfield',
    '#default_value' => isset($items[$delta]['path']) ? $items[$delta]['path'] : '',
    '#autocomplete_path' => $instance['widget']['settings']['autocomplete_path'] . '/' . $field['field_name'],
    '#size' => $instance['widget']['settings']['size'],
    //'#element_validate' => array('pathreference_autocomplete_validate'),
  );

  return $element;
}

/**
 * Retrieve a JSON-array of autocomplete suggestions
 */
function pathreference_autocomplete($field_name, $string = '') {
  $field = field_info_field($field_name);
  $matches = array();

  //$string = substr($_GET['q'], strlen('pathreference/autocomplete/'.$field_name.'/'));
  $len = strlen($string);
  $parts = substr_count($string, '/');

  /*$menu = menu_get_menu();

  foreach ($menu['visible'] as $mid => $item) {
    if (substr($item['path'], 0, $len) == $string) {
      $matches[$item['path']] = $item['path'];
    //$matches[$row->node_title .' [nid:'. $row->nid .']'] = _nodereference_item($field, $row);
    }
  }*/

  $sql = "SELECT * FROM {url_alias} WHERE SUBSTR(dst,1,%d) = '%s'";
  $result = db_query($sql, $len, $string);

  while ($data = db_fetch_object($result)) {
    if (substr_count($data->dst, '/')-1 <= $parts)
    $matches[$data->dst] = $data->dst;
  }
  //$matches['test/path'] = 'test/path';

  //$matches = $menu['path index'];

  sort($matches);
  // Only return the first 25 matches
  array_splice($matches, 25);

  drupal_json_output($matches);
}

/**
 * Create an array of the allowed paths for this field
 */
function pathreference_allowed_paths($field) {
  foreach (explode("\n", $field['settings']['allowed_paths']) as $path) {
    $path = trim($path);
    if (strlen($path)) {
      $allowed_paths[] = $path;
    }
  }

  return $allowed_paths;
}
